/**
 * Aggregates the opinions between each groups (and itself), 
 * and returns in a 3d array giving the list of opinions from the group
 * in the first dimension to the group in the second dimension. 
 * @param {Population} pop Population object 
 */
function opinionsBetweenGroup(pop) {
    let opinions = Array.from(Array(pop.nbGroups), () => Array.from(Array(pop.nbGroups), () => Array()));
    pop.pop.forEach(indA => {
        indA.opinion.forEach((opinion, indB_ID) => {
            opinions[indA.groupID][pop.pop[indB_ID].groupID].push(opinion);
        })
    });
    return opinions;
}

/**
 * Returns an array of reputations for each individual.
 * 
 * @param {Population} pop 
 * @returns an array
 */
function reputations(pop) {
    return pop.pop.map(indA => indA.getReputation(pop));
}

/**
 * Computes the rank of reputations of individuals and return these ranks for each group. 
 * @param {Population} pop 
 * @returns an array of array of ranks
 */
function ranksByGroups(pop) {
    // first, we need the reputations. We build a list of object containing the individual and its reputation
    let reputations = pop.pop.reduce(function(reputations_,ind) {
        reputations_.push({"ind":ind, "rep":ind.getReputation(pop)});
        return reputations_;
    }, Array());
    // then, we sort by reputation (decreasing order)
    reputations.sort((a,b) => b.rep - a.rep);
    // now we can get ranks
    let lastRank = 0;
    let lastValue = pop.MIN_OPINION - 1;
    let ranksByGroups = Array.from(Array.from({length: pop.nbGroups}),() => Array());
    reputations.forEach(function(val,index) {
        // two individuals with same reputation will have the same rank
        if (val.rep!=lastValue) {
            lastRank++;
        }
        val.rank = lastRank;
        // append the rank in the list of ranks of the group
        ranksByGroups[val.ind.groupID].push(val.rank);
        lastValue = val.rep;
    });
    return ranksByGroups;
}

class RanksTransitionMatrix {

    /**
     * Comparators function used for selecting rank transitions.
     */ 
    static comparators = {
        gt: (a,b) => a > b,
        ge: (a,b) => a >= b,
        lt: (a,b) => a < b,
        le: (a,b) => a <= b,
        eq: (a,b) => a == b
    }

    constructor(pop) {
        this.previousRanks = undefined;
        this.transitionsCount = Array.from({length: pop.nbGroups}, () => Array.from({length: pop.sizeP}, () => Array.from({length: pop.sizeP}, () => 0)));
        this.MIN_OPINION = pop.MIN_OPINION;
    }

    /**
     * Process population state for this iteration: count ranks transitions
     * @param {*} pop 
     */
    processStep(pop) {
        // first, we need the reputations. We build a list of object containing the individual and its reputation
        let reputations = pop.pop.reduce(function(reputations_,ind) {
            reputations_.push({"ind":ind, "rep":ind.getReputation(pop)});
            return reputations_;
        }, Array());
        // then, we sort by reputation (decreasing order)
        reputations.sort((a,b) => b.rep - a.rep);
        // now we can get ranks
        let lastRank = 0;
        let lastValue = pop.MIN_OPINION - 1;
        const firtStep = (this.previousRanks === undefined);
        if (firtStep) this.previousRanks = Array.from({length: pop.sizeP}, () => 0);
        reputations.forEach(function(val,index) {
            // two individuals with same reputation will have the same rank
            if (val.rep!=lastValue) {
                lastRank++;
            }
            val.rank = lastRank;
            // append the rank in the list of ranks of the individual
            if (!firtStep) {
                this.transitionsCount[val.ind.groupID][this.previousRanks[val.ind.id]-1][lastRank-1]++;
            }
            this.previousRanks[val.ind.id] = lastRank;
            lastValue = val.rep;
        }, this);
    }
   
    /**
     * Returns the number of transitions from a rank to another, for each group.
     * 
     * @returns A matrix nbGroups x nbIndividuals x nbIndividuals
     */
    getMatrices() {
        return this.transitionsCount;
    }

    /**
     * Returns the probas of transition knowing a rank to another, for each group.
     * 
     * @returns A matrix nbGroups x nbIndividuals x nbIndividuals
     */
    getProbasMatrices() {
        return this.getMatrices().map((grp) => grp.map((row) => {
            const sum = row.reduce((prev,cur) => prev+cur, 0);
            return row.map((elt) => (sum===0) ? 0 : elt/sum);
        }));
    }

    /**
     * Returns the probas of transition knowing a rank to another, for each group.
     * 
     * @param {function} comparator A comparator function returning the comparison between two elements, from those provided in RanksTransitionMatrix.comparators.
     * @returns A matrix nbGroups x nbIndividuals x nbIndividuals
     */
     getProbasChangingRank(comparator) {
        return this.getMatrices().map((grp) => grp.map((row, fromIndex) => {
            const sumChanging = row.filter((elt, toIndex) => comparator(toIndex, fromIndex)).reduce((prev,cur) => prev+cur, 0);
            const sum = row.reduce((prev,cur) => prev+cur, 0);
            return (sum===0) ? 0 : sumChanging/sum;
        }));
    }
}
