// This sample can be run with command:
// cat indicators.js population.js batch-test.js | node -

const seedrandom = require('seedrandom');
const rng = seedrandom('a fixed status');

sizesPop = [20, 20]
ka = [3, 3] // 0, 1, 2 or 3 nb of targets for gossip
delta = [0.2, 0.2] // fixed to 0.2
min_opinion = -5.0 // fixed to -5.0
probasIntergroupContact = [0.5, 0.5]
sigmas = [0.2, 0.3]
omegas = [0, 0] // vanity disabled
rhos = [1,1] // full intensity
pop = new Population(sizesPop, omegas, sigmas, probasIntergroupContact, ka, rhos, delta, min_opinion, rng);

nbSteps = 500000;
verboseOutput = true;

transitionsMatrix = new RanksTransitionMatrix(pop);
start = new Date().getTime();
[...Array(nbSteps)].map(function() {
    pop.step();
    transitionsMatrix.processStep(pop);
});
elapsed = new Date().getTime() - start;
console.log(nbSteps + " steps done in " + elapsed +"ms");

if (verboseOutput) {
    console.log("Final state of opinions:")
    console.log(pop.toString());
    console.log("number of unchanged opinions = "+ pop.pop.reduce(
        (total,current) => total+current.opinion.reduce(
            (total, current) => (current===min_opinion)?total+1:total
            ,0),0));

    console.log("Reputations: " + reputations(pop).map(rep => rep.toFixed(1)).join(", "))
    console.log("Ranks by groups: \n  " + ranksByGroups(pop).join('\n  '));

    transitions = transitionsMatrix.getMatrices();
    [0,1].forEach(function(grp) {
        console.log("Transitions counts group "+(grp+1)+":")
        console.table(transitions[grp]);    
        console.log("transitionsCount group "+(grp+1)+":")
        console.table(transitionsMatrix.transitionsCount[grp]);    
    });

    transitions = transitionsMatrix.getProbasMatrices();
    [0,1].forEach(function(grp) {
        console.log("Transitions Probas group "+(grp+1)+":")
        console.table(transitions[grp].map((row) => row.map((elt) => (elt.toFixed(3)).slice(-5))));    
    });

    start = new Date().getTime();
    transitions = transitionsMatrix.getProbasChangingRank(RanksTransitionMatrix.comparators.le);
    elapsed = new Date().getTime() - start;
    console.log("probas computed in " + elapsed +"ms");
    [0,1].forEach(function(grp) {
        console.log("Probas to move to lower rank group "+(grp+1)+":")
        console.log(transitions[grp].map((elt) => (elt.toFixed(2)).slice(-4)).join(" ")); 
    });
}