// This sample can be run with command:
// cat indicators.js population.js batch-matrices.js | node -

var seedrandom = require('seedrandom');
var myrng = seedrandom('1');

sizesPop = [20, 20]
ka = [3, 3] // 0, 1, 2 or 3 nb of targets for gossip
delta = [0.2, 0.2] // fixed to 0.2
min_opinion = -5.0 // fixed to -5.0
probasIntergroupContact = [0.5, 0.5]
sigmas = [0.2, 0.3]
nbSteps = 500000;
nbStepsWarmup = 100000; // transitions will not be counted during this phase

const omegas = [0, 0] // vanity disabled
const rhos = [1,1] // full intensity
                
outfilePrefix = '../out/output_'
// init the output dir
const fs = require('fs')
fs.mkdir(outfilePrefix.substr(0, outfilePrefix.lastIndexOf('/')), { recursive: true }, (err) => {
    if (err && err.code!=="EEXIST") {
        return console.error(err);
    }
})

// init the model
pop = new Population(sizesPop, omegas, sigmas, probasIntergroupContact, ka, rhos, delta, min_opinion, myrng);
// init the transitions matrices holder
transitionsMatrix = new RanksTransitionMatrix(pop);
// run the simulation
start = new Date().getTime();
[...Array(nbSteps)].map(function(_,it) {
    pop.step();
    if (it>nbStepsWarmup) {
        transitionsMatrix.processStep(pop);
    }
});
elapsed = new Date().getTime() - start;
console.log(nbSteps + " steps done in " + elapsed +"ms");

// write transitions matrices
transitions = transitionsMatrix.getProbasMatrices();
[0,1].forEach(function(grp) {
    fs.writeFile(outfilePrefix+grp+'.txt', transitions[grp].map((row) => row.join(",")).join("\n"), (err) => {if (err) throw err})
});

