# gender-diff-model

Web application for playing with the [gender differenciation model published by S. Huet, F. Garguilo and F. Pratto in 2020](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0236840).

## Running the raw javascript model

An sample scrit is given in ```js/batch-test.js``` and can be run with ```cat indicators.js population.js batch-test.js | node -```.

## Simple HTML

The code in ```simple-html``` directory is a simple version using Jquery/bootstrap for the UI and plotly for the plots.
You can test it by:
- copy files from ```js``` directory to ```simple-html/js/``` directory
- running ```python3 -m http.server``` in the ```simple-html``` directory
- visiting with your browser http://0.0.0.0:8000/simple-html/

See [the documentation of the application](simple-html/Readme.md).
