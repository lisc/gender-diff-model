# Web application for the Gender Differentiation Model

This is a web application using the javascript implementation of the Gender Differentiation Model.

The conception remains in 3 parts:
- a user interface for setting parameters and running the simulator
- a set of plots for visualizing output indicators
- a documentation part

In the directory ```js/``` you will find third-party dependencies (bootstrap, jquery and plotly) and following javascript files:
- bibliography.js: used for displaying bibliographic references in the documentation
- plots.js: contains the class structure and basic implementations for plots
- presets.js: used for managing presets for simulator's parameters in the documentation
- simulator.js: all the controller part

Here is a sequence diagram of user interaction and controller effects:
![](doc/sequence.png)

## Adding a plot implementation

It requires to:
1. implement a new class that inherits from the class Plot in the script plot.js
2. add an instance of this plot in the list defined in index.html (look for ```const plots = new Plots([]))```).