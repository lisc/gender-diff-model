/**
 * Fill the configuration form with a list of parameters, their initial values,
 * and their bounds.
 * 
 * For each parameter in parameters:
 *  - the key will determine the id of the input element ("args-"+key)
 *  - a dict object giving the ```label```, de ```min``` value, the ```max```,
 *    the ```default``` and the ```step``` increment/decrement.
 * 
 * @param {string} configPanel - the ID of the DOM element to populate
 * @param {Object} parameters - a dict of the parameters.
 */
function populateConfigPanel(configPanelID, parameters) {
    const configPanel = $('#config');
    for ([param, values] of Object.entries(parameters)) {
        configPanel.children('form').append('<div class="form-group"><label for="args-'
            + param + '"class="col-sm-4 param">' + values.label
            + '</label><div class="col-sm-4"><input type="number" class="form-control" id="args-'
            + param + '" step="' + values.step + '" min="' + values.min + '" max="' + values.max + '"></div></div>');
    }
    configPanel.on('change', 'input', function () {
        updatePermalink();
        if (['args-nbSteps', 'args-popsize'].indexOf($(this).attr('id')) < 0) {
            updatePresetRecall($('#presetRecall'), '');
        }
    });
}
/**
 * Update the permalink with the value of parameters currently setted.
 */
function updatePermalink() {
    let urlHash = "";
    $('#config input').each(function (input) {
        const name = $(this).attr('id').replace('args-', '');
        urlHash += name + "=" + $(this).val() + "&";
    });
    urlHash += "plots=" + $('#plots-frequency input:checked').attr('value');
    // Adding preset identifiant if currently active
    const presetName = $('#presetRecall').attr('presetName');
    if (presetName) {
        urlHash += "&preset=" + presetName;
    }
    $('#permalink').attr('href', "#" + urlHash);
}
/**
 * Resets to initial default parameters
 */
function resetParameters(parameters) {
    for ([param, values] of Object.entries(parameters)) {
        $('#args-' + param).val(values.default);
    }
    // force slider update
    $('#args-nbSteps').trigger("input");
}

class Simulator {
    /**
     * Creates the simulator and the UI events.
     * @param {Plots} plots
     * @param {*} parameters 
     */
    constructor(plots, parameters, nbIterationByStep, nbStepsWarmup) {
        this.plots = plots;
        this.nbIterationByStep = nbIterationByStep;
        this.nbStepsWarmup = nbStepsWarmup;
        this.simulationRun = true;
        this.stopIteration = 0;
        this.paused = false;
        this.iteration = 0;
        this.rngseed = null;
        this.pop;
        this.timeserieStep = 1;
        this.stepsDuration = 0;
        this.plotDuration = -1;
        this.nextPlotsTimestep = -1;
        // controller components
        this.playBtn = $('#play-btn');
        this.pauseBtn = $('#pause-btn');
        this.stopBtn = $('#stop-btn');
        this.itSlider = $("#iterationSlider");
        this.itSliderMax = $("#iterationSlider-max");
        this.itSliderVal = $("#iterationSlider-val");
        // Events for the 3 big buttons in the top jumbotron
        $('#btnNavPlay').click(function () {
            $('html, body').animate({ scrollTop: $('#simulator').position().top }, 500);
        });
        $('#btnNavLearn').click(function () {
            $('html, body').animate({ scrollTop: $('#documentation').position().top }, 500);
        });
        $('#btnNavAbout').click(function () {
            $('html, body').animate({ scrollTop: $('#about').position().top }, 500);
        });
        // Events for the simulator buttons
        this.playBtn.click(() => {
            if (this.simulationRun) {
                this.pause();
                this.plots.updateIndicators(this.iteration);
                //updatePlotIndicators(iteration);
            } else {
                //$('html, body').animate({ scrollTop: $('#simulator').position().top }, 500);
                $('#play-btn span').removeClass('glyphicon-play');
                $('#play-btn span').addClass('glyphicon-pause');
                $('#resetParameters').hide();
                $('#config input').prop("disabled", true);
                $('#args-nbSteps').prop("disabled", false);
                $('#config select').prop("disabled", true);
                $('#tabs a[href="#plots"]').tab('show');
                this.simulationRun = true;
                if (!this.paused) {
                    this.stopIteration = this.iteration + parseInt($('#args-nbSteps').val(), 10);
                    this.itSlider.attr('max', this.stopIteration);
                    this.itSliderMax.text(this.stopIteration);
                } else {
                    this.paused = false;
                }
                // will create a thread for simulation without blocking current thread
                // we use the bind function to pass the current this to the context of the called function
                setTimeout(this.simulation.bind(this), 0);
                setTimeout(this.plotsRefreshLoop.bind(this), 0);
            }
        });
        this.stopBtn.click(this.pause);
        $('#stop-confirmed').click( () => {
            this.resetSimulation();
            $('#config input').prop("disabled", false);
            $('#config select').prop("disabled", false);
            $('#resetParameters').show();
            $('#tabs a[href="#config"]').tab('show');
            $('#confirm-stop').modal('hide');
        });
        $('#resetParameters').on('click', () => {
            this.resetParameters();
            window.location.hash = "";
            updatePermalink();
        });
        $('#args-nbSteps').on("input", () => {
            const step = this.iteration + parseInt(this.value);
            this.itSlider.attr('max', step);
            this.itSliderMax.text(step);
            this.stopIteration = step;
        });
        $('#plots-frequency').on('change', 'input', () => {
            updatePermalink();
        });
        this.itSlider.on("change", () => {
            if (this.value <= this.iteration) {
                plots.updateIndicators(parseInt(this.value));
                // Maybe delegate this call with a timer for avoiding UI freeze?
                plots.refresh(parseInt(this.value));
            }
        });
        this.itSlider.on("input", () => {
            if (this.value > this.iteration) {
                this.itSlider.val(this.iteration);
            } else {
                this.itSliderVal.text(this.value);
            }
        });
        $('#plots-frequency input').on('change', () => {
            this.computeNextPlotsTimestep($('#plots-frequency input:checked').attr('value'));
        });
        resetParameters(parameters);
        this.loadURLArguments();
        this.resetSimulation();
    }
    
    /**
     * Initialize the parameters with arguments given in the URL
     */
    loadURLArguments() {
        // load URL arguments
        const params = (window.location.hash.substring(1)).split("&");
        for (let i = 0; i < params.length; i++) {
            const a = params[i].split("=");
            if (a[0] === "plots") {
                $('#plots-frequency input[value=' + a[1] + ']').attr('checked', true);
                $('#plots-frequency input').parent().removeClass("active");
                $('#plots-frequency input[value=' + a[1] + ']').parent().addClass("active");
            } else if (a[0] === "rngseed") {
                this.rngseed = a[1];
            } else if (a[0] === "preset") {
                // Spaces char in the preset name will be replaced in the URL by %20, so we fix them
                $('#presetRecall').attr('presetName', a[1].replace(/%20/g, ' '));
            } else {
                $('#args-' + a[0]).val(a[1]);
            }
        }
        updatePermalink();
    }

    /**
     * Called on load and when the user has stopped the simulation
     * for resetting the simulator's variables.
     */
    resetSimulation() {
        this.plots.empty();
        this.timeserieStep = 1;
        this.simulationRun = false;
        this.iteration = 0;
        this.stepsDuration = 0;
        this.plotDuration = -1;
        this.nextPlotsTimestep = -1;
        this.stopIteration = this.iteration + parseInt($('#args-nbSteps').val());
        this.itSlider.val(0);
        this.itSliderVal.text('0');
        this.itSlider.attr('max', this.stopIteration);
        this.itSliderMax.text(this.stopIteration);
        updatePermalink();
    }

    /**
     * Computes at which timestep the plots need to be refreshed, depending on
     * the frequency mode currently selected.
     * @param {string} frequency the frequency mode currently selected ("auto", "off"
     *  or the number of steps)
     */
    computeNextPlotsTimestep(frequency) {
        if (frequency === 'auto') {
            // Ploting time should not exceed 30 times the time spended for simulation
            this.nextPlotsTimestep = this.iteration
                + Math.max(Math.round((30 * this.plotDuration) / (this.stepsDuration / this.iteration) ), 1);
        } else if (frequency === 'off') {
            this.nextPlotsTimestep = -1;
        } else {
            this.nextPlotsTimestep = this.iteration + parseInt(frequency, 0);
            if (this.iteration === 1 && this.nextPlotsTimestep > this.iteration + 1) {
                // fix for first iteration, for avoiding disgracious shift
                this.nextPlotsTimestep -= 1;
            }
        }
    }
    /**
     * Starts a loop for watching if new data is available for updating the plot.
     */
    plotsRefreshLoop() {
        const start = new Date().getTime();
        if (this.plots.refresh(this.iteration)) {
            // if the plots have been updated, we update the duration indicator
            this.plotDuration = new Date().getTime() - start;
        }
        if (this.simulationRun) {
            setTimeout(this.plotsRefreshLoop.bind(this), 2000);
        }
    }
 
    
    /**
     * Simulation loop that initalizes the model at the first step and then run
     * the simulation.
     */
    simulation() {
        if (this.simulationRun) { // token used for repeating the steps. Will stop if token is disabled by another action
            if (this.iteration === 0) {
                // initialization step
                const omegas = [0, 0]; // vanity disabled
                const rhos = [1,1]; // full intensity
                const deltas = [0.2,0.2];
                this.pop = new Population(
                    [parseInt($('#args-popsize1').val()), parseInt($('#args-popsize1').val())],
                    omegas,
                    [parseFloat($('#args-sigma1').val()), parseFloat($('#args-sigma2').val())],
                    [parseFloat($('#args-probaint1').val()), parseFloat($('#args-probaint2').val())],
                    parseInt($('#args-k').val()),
                    rhos, deltas, -5, this.rngseed);
                this.transitionsMatrix = new RanksTransitionMatrix(this.pop);
                // draw the plots and measure the time to get them (used for auto-refresh frequency)
                const start = new Date().getTime();
                this.plots.init(this);
                this.plotDuration = new Date().getTime() - start;
            }
            const start = new Date().getTime();
            [...Array(this.nbIterationByStep)].map(() => {
                this.iteration ++;
                this.pop.step();
                if (this.iteration>this.nbStepsWarmup) {
                    this.transitionsMatrix.processStep(this.pop);
                }
            })
            this.stepsDuration = this.stepsDuration + (new Date().getTime() - start);
            // update the plots if required
            this.plots.updateIndicators(this.iteration);
            const frequency = $('#plots-frequency input:checked').attr('value');
            if (frequency !== "off") {
                if (this.nextPlotsTimestep === -1) {
                    this.computeNextPlotsTimestep(frequency);
                }
                if (this.iteration >= this.nextPlotsTimestep) {
                    // FIXME separate the updating process for indicators and the refresh of plots
                    //this.plots.updateIndicators(this.iteration);
                    this.computeNextPlotsTimestep(frequency);
                }
            }
            // update progress bar
            this.itSlider.val(this.iteration);
            this.itSliderVal.text(this.iteration);
            this.simulationRun = this.iteration < this.stopIteration;
            // next iteration
            setTimeout(this.simulation.bind(this), 0);
        } else {
            // Configured nb steps reached
            //this.plots.updateIndicators(this.iteration);
            $('#play-btn span').removeClass('glyphicon-pause');
            $('#play-btn span').addClass('glyphicon-play');
        }
    }

    pause() {
        $('#play-btn span').removeClass('glyphicon-pause');
        $('#play-btn span').addClass('glyphicon-play');
        this.simulationRun = false;
        this.paused = true;
    }
    
}

