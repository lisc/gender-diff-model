class Plot {
    constructor(graphDiv) {
        this.graphDiv = graphDiv;
     }

    _init(simulator) {
        this.simulator = simulator;
        this.init(simulator);
    }

    init(simulator) { }

    updateIndicators(iteration) { }

    refresh(iteration, simulator) { }
}
class Plots {

    constructor(plots) {
        this.plotsToRefresh = -1;
        this.plots = plots;
    }

    empty() {
        $('#plot').empty();
    }

    init(simulator) {
        this.simulator = simulator;
        this.empty();
        this.plots.forEach(plot => {
            $('#plots').append('<div class="col-lg-6" id="' + plot.graphDiv + '"></div>');
            plot._init(simulator);
        });
    }

    /**
     * Invokes refresh of plots if indicators have been updated.
     * 
     * @param {int} iteration Current timestep value
     */
    refresh(iteration) {
        if (this.plotsToRefresh > -1) {
            this.plots.forEach(plot => {
                plot.refresh(this.plotsToRefresh, this.simulator);
            });
            this.plotsToRefresh = -1;
            return true;
        }
        return false;
    }

    /**
     * For appending new data for the plots.
     * @param {} iteration 
     */
    updateIndicators(iteration) {
        this.plotsToRefresh = iteration;
        this.plots.forEach(plot => {
            plot.updateIndicators(iteration);
        });
    }
}

class ReputationsSeriePlot extends Plot {
    constructor(graphDiv) {
        super(graphDiv);
    }

    init(simulator) {
        const colors = ['#1f77b4', '#ff7f0e'];
        const legends = [0,1].map(grp => 'G' + grp + ' (σ='+simulator.pop.groupsParams[grp].sigma+')');
        const showLegendIndex = [0]; // init with first individual
        // now looking for the index of the first individual of the other group
        let idx = 1;
        while (simulator.pop.pop[idx].groupID == simulator.pop.pop[0].groupID) {
            idx++;
        }
        showLegendIndex.push(idx);
        // building the traces
        this.traces = simulator.pop.pop.map((ind) => ({
            ind: ind,
            x:[simulator.iteration],
            y:[ind.getReputation(simulator.pop)],
            type:"scatter",
            mode:"lines",
            showlegend: showLegendIndex.includes(ind.id),
            name: legends[ind.groupID],
            line: {
                color:colors[ind.groupID],
                width:1
            }
        }));
        Plotly.newPlot(this.graphDiv, this.traces, {
            width: 600, height: 550, yaxis: {
                title: 'Reputations traces'
            }
        });
    }

    updateIndicators(iteration) {
        this.traces.forEach(function(elt) {
            elt.x.push(this.simulator.iteration);
            elt.y.push(elt.ind.getReputation(this.simulator.pop));
        }.bind(this));
    }

    refresh(iteration, simulator) {
        // FIXME redraw doesn't work, and recreating plot is OK.
        Plotly.newPlot(this.graphDiv, this.traces, {
            width: 600, height: 550, yaxis: {
                title: 'Reputations traces'
            }
        });
        //Plotly.redraw(this.graphDiv, this.buildBoxplotData(population));
    }

}

class SimplePlot extends Plot {
    constructor(graphDiv) {
        super(graphDiv);
    }

    buildBoxplotData(population) {
        const ops = opinionsBetweenGroup(population);
        const traces = Array.from({ length: population.nbGroups * population.nbGroups },
            (none, index) => {
                const grpFrom = Math.floor(index / population.nbGroups);
                const grpTo = index % population.nbGroups;
                return {
                    name: 'G' + grpFrom + ' on G' + grpTo,
                    type: 'box',
                    y: ops[grpFrom][grpTo]
                };
            });
        return traces;
    }

    init(simulator) {
        Plotly.newPlot(this.graphDiv, this.buildBoxplotData(simulator.pop), {
            width: 600, height: 550, yaxis: {
                title: 'Intergroups esteems'
            }
        });
    }

    updateIndicators(iteration) {
        // TODO plots indicators at iteration
    }

    refresh(iteration, simulator) {
        // FIXME redraw doesn't work, and recreating plot is OK.
        Plotly.newPlot(this.graphDiv, this.buildBoxplotData(simulator.pop), {
            width: 600, height: 550, yaxis: {
                title: 'Intergroups esteems'
            }
        });
        //Plotly.redraw(this.graphDiv, this.buildBoxplotData(population));
    }

}

class PlotGroupsDistribution extends Plot {
    /**
     * Build a plot for representing the distribution of an individual's indicator for each group.
     * 
     * @param {String} graphDiv The ID of the div element that will be created
     * @param {*} plotTitle The title of the plot
     * @param {*} fn The function that will be called on population to get an array of individuals indicators for each group.
     */
    constructor(graphDiv, plotTitle, fn) {
        super(graphDiv);
        this.plotTitle = plotTitle;
        this.fn = fn;
    }

    buildBoxplotData(pop) {
        const data = this.fn.call(null, pop);
        return data.map(function(groupData, i) { return {name: 'G' + i + ' (σ='+pop.groupsParams[i].sigma+')', type: 'box', y: groupData}});
    }

    init(simulator) {
        Plotly.newPlot(this.graphDiv, this.buildBoxplotData(simulator.pop), {
            width: 600, height: 550, yaxis: {
                title: this.plotTitle
            }
        });
    }

    updateIndicators(iteration) {
        // TODO plots indicators at iteration
    }

    refresh(iteration, simulator) {
        // FIXME redraw doesn't work, and recreating plot is OK.
        Plotly.newPlot(this.graphDiv, this.buildBoxplotData(simulator.pop), {
            width: 600, height: 550, yaxis: {
                title: this.plotTitle
            }
        });
        //Plotly.redraw(this.graphDiv, this.buildBoxplotData(population));
    }

}

class ProbaChangingRankPlot extends Plot {

    constructor(graphDiv, comparator) {
        super(graphDiv);
        this.comparator = comparator;
        this.title = 'Probabilities of transition to ' + {
            "le": 'a lower or equal rank',
            "lt": 'a lower rank',
            "eq": 'an equal rank',
            "ge": 'a greater or equal rank',
            "gt": 'a greater rank',            
        }[this.comparator.name];
    }

    init(simulator) {
        this.ranksRange = Array.from({ length: simulator.pop.sizeP}, (_, i) => 1 + i);
    }

   refresh(iteration, simulator) {
        const probasByGroup = simulator.transitionsMatrix.getProbasChangingRank(this.comparator);
        Plotly.newPlot(this.graphDiv, [
            { x: this.ranksRange, y: probasByGroup[0], mode: 'lines+markers',type: 'scatter', name: 'G0 (σ='+simulator.pop.groupsParams[0].sigma+')' },
            { x: this.ranksRange, y: probasByGroup[1], mode: 'lines+markers',type: 'scatter', name: 'G1 (σ='+simulator.pop.groupsParams[1].sigma+')' }
        ], { yaxis: { title: this.title } });
        // Force the autorange after the drawing process
        Plotly.relayout(this.graphDiv, {
            'xaxis.autorange': true,
            'yaxis.autorange': true
        });
    }

}