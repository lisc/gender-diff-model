/**
 * 'presets' contains all the parameters presets indexed by their id.
 * each preset is an array containing first the name of the preset and
 * then the parameter values.
 * The dict will be filled by the function ```add_presets```.
 */
const presets = {};
/**
 * if a valid preset's name is given, it will display (above the parameters tab)
 * the name of this preset to recall to the user that it has been selected.
 * As soon as a parameter will be modified (except popsize and nbsteps), the message
 * will be cleared.
 * @param {type} presetName
 */
function updatePresetRecall(presetRecallElt, presetName) {
    const presetRecall = presetRecallElt.attr('presetName');
    if (presetRecall !== presetName) {
        if (presetName.length>0) {
            presetRecallElt.text("Current preset: "+presetName);
            presetRecallElt.delay(500).animate({ backgroundColor: "#d9edf7" }, 1500 );
            presetRecallElt.delay(2000).animate({ backgroundColor: "#FFFFFF" }, 500 );
        } else if (presetRecall) {
            presetRecallElt.html("<b>Leaving</b> preset: "+presetRecall+" (parameters modified)");
            presetRecallElt.animate({ backgroundColor: "#f2dede" }, 500 );
            presetRecallElt.delay(3000).animate({ backgroundColor: "#FFFFFF" }, 1500 );
            setTimeout( function(){ presetRecallElt.text(''); }, 5000 );
        }
        presetRecallElt.attr('presetName', presetName);
        updatePermalink();
    }
}

/**
 * Fill a table with given parameters and add a button for using
 *  these parameters in the simulator.
 * @param {type} tableID
 * @param {type} data
 * @returns {undefined}
 */
function add_presets(tableID, data) {
    const table = document.querySelector("#"+tableID+" tbody");
    // adding header by retrieving parameters lables, except nbSteps
    paramsElt = $("#config .param:not([for=args-nbSteps])");
    paramsKeys = paramsElt.map(function() {return $(this).attr("for")}).get();
    paramsLabels = paramsElt.map(function() {return $(this).html()}).get();
    data = [["Configuration name",].concat(paramsLabels)].concat(data);
    //Converting rows to columns
    const r = data[0].map(function(col, i) {
        return data.map(function(row) {
            return row[i];
        });
    });
    // filling array
    r.forEach(function (row) {
        let tr = '<tr>';
        row.forEach(function (cell, i) {
            if (i === 0) {
                tr += '<th scope="row">' + cell + '</th>';
            } else {
                tr += '<td>' + cell + '</td>';
            }
        });
        table.innerHTML += tr + '</tr>';
    });
    // adding buttons
    let row = '<tr>';
    data.forEach(function (elt, i) {
        if (i === 0) {
            row += '<th scope="row">Click for using these parameters</th>';
        } else {
            const slug = elt[0].replace(/[^a-z0-9-]/gi, '-');
            presets[slug] = elt;
            const id = "parameters" + slug;
            row += '<td><button id="' + id + '" type="button" class="btn btn-primary">' + elt[0] + '</button></td>';
            $("#documentation").on("click", "button#" + id, function (clickEvent) {
                clickEvent.preventDefault();
                paramsKeys.forEach( (param, index) => {
                    $('#'+param).val(elt[1+index]);
                });
                // force slider update
                $('#args-nbSteps').trigger("input");
                // FIXME strange scroll behaviour. It should give the simulator buttons on the top.       
                //$('html, body').animate({scrollTop: $('#simulator').position().top}, 500);
                updatePresetRecall($('#presetRecall'), elt[0]);
            });
        }
    });
    table.innerHTML += row + '</tr>';
}