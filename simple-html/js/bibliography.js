const references = {
    "Huet2020":{ cite: "Huet, Gargiulo and Pratto, 2020", text: "Huet S, Gargiulo F, Pratto F (2020) Can gender inequality be created without inter-group discrimination? PLOS ONE 15(8): e0236840.", link: "https://journals.plos.org/plosone/article/citation?id=10.1371/journal.pone.0236840"}
};
$("#documentation .bib").each(function(e){
    const ref = references[$(this).attr('bib')];
    //console.log($(this).attr('bib'), ref);
    $(this).text(ref.cite);
    if ('link' in ref) {
        $(this).attr('title', '<p>'+ref.text+'</p><p><a href="'+ref.link+'" target="_blank">Read online</a></p>');
        $(this).tooltip({html:true, delay: {show:500, hide:800}});
    } else {
        $(this).attr('title', ref.text);
        $(this).tooltip({delay: {show:500, hide:800}});
    }
});
const referencesList=$('#references');
$.each(references, function(i, ref){
    if ('link' in ref) {
        referencesList.append('<dt>'+ref.cite+'</dt><dd>'+ref.text+' <a href="'+ref.link+'" target="_blank">Read online</a></dd>');
    } else {
        referencesList.append('<dt>'+ref.cite+'</dt><dd>'+ref.text+'</dd>');
    }
});
